package zj.loggy.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import zj.loggy.db.entity.LogEntity

@Database(entities = [LogEntity::class], version = 1, exportSchema = false)
abstract class LoggyDataBase: RoomDatabase() {

    abstract fun appLogDao(): AppLogDao

    companion object {
        private var INSTANCE: LoggyDataBase? = null

        fun getInstance(context: Context): LoggyDataBase? {
            if (INSTANCE == null) {
                synchronized(LoggyDataBase::class) {
                    val klass = LoggyDataBase::class.java
                    val dbName = "logi.db"
                    INSTANCE = Room.databaseBuilder(context, klass, dbName).build()
                }
            }
            return INSTANCE
        }

        fun destroyInstance() {
            INSTANCE?.close()
            INSTANCE = null
        }
    }
}