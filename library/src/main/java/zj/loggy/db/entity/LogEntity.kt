package zj.loggy.db.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class LogEntity(
    @PrimaryKey(autoGenerate = true) var id: Int? = null,
    var log: String
)