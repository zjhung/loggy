# Gradle
```
dependencies {
    implementation 'zj.loggy:loggy:x.x.x'
}
```

# What Does The Library Do
Loggy let you log arbitrary user actions with automatically attached app version, network type, device info and you can append extra information to the log. 
Additionally, You can group actions into main categories and sub categories.

Loggy serves as local logging library, we don't upload any data. 
Instead, we keep the logs in a database, so that you can decide when and where you upload logs to.

Loggy doesn't have default limit size for logs, but you can define `tableSize` to discard old logs when logs grow too large.

# How To Use
## Insert
- Create you Action, for example:
```
val info = if (throwable != null) {
    mapOf("name" to apiName,
            "statusCode" to response?.code().toString(),
            "desc" to throwable.message)
} else {
    val rsp = response?.body() as? ResponseData
    mapOf("name" to apiName,
            "statusCode" to response?.code().toString(),
            "returnCode" to rsp?.returnCode,
            "desc" to rsp?.description)
}
val action = Action("API", null, info)
```
- Create new AppLog, and optionally attach extra info
```
LoggyProvider.newLog(action).also {
    it.append("message", "Hello")
}
```
- Save log
```
LoggyProvider.save(listOf(log))
```
## Fetch
```
LoggyProvider.fetch(size)
```


Any opinions and suggestions are welcome. Thanks.