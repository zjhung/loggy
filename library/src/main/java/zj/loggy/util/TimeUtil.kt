package zj.loggy.util

import java.text.SimpleDateFormat
import java.util.*


object TimeUtil {

    private const val UTC_FORMAT = "yyyy-MM-dd'T'HH:mm:ss'Z'"

    data class LogTime(val utcTime: String, val timeZone: String)

    fun getUTCTimeWithTimeZone(): LogTime {
        val calendar = Calendar.getInstance(
            TimeZone.getTimeZone("GMT"),
            Locale.getDefault()
        )
        val localTime = calendar.time

        val utcFormatter = SimpleDateFormat(UTC_FORMAT, Locale.getDefault())
        utcFormatter.timeZone = TimeZone.getTimeZone("UTC")

        val gmtFormatter = SimpleDateFormat("Z", Locale.getDefault())

        return LogTime(utcFormatter.format(localTime), gmtFormatter.format(localTime))
    }
}