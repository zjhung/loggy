object Config {

    const val artifactName = "loggy"
    const val versionCode = 11
    const val versionName= "0.0.11"

    const val compileSdk = 30
    const val targetSdk = 30
    const val minSdk = 23
}