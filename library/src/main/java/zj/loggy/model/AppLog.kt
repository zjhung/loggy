package zj.loggy.model

import com.google.gson.GsonBuilder
import com.google.gson.annotations.Expose
import zj.loggy.util.TimeUtil
import java.util.*


class AppLog
internal constructor(
    @Expose var id: Int? = 0,
    @Expose var timestamp: String = "",
    @Expose var timezone: String = "",
    @Expose var appVersion: String = "",
    @Expose var networkMedia: String = "N/A",
    @Expose var device: Device? = null,
    @Expose var userInfo: User? = null,
    @Expose var action: Action? = null,
    @Expose private var extra: MutableMap<String, Map<String, Any?>?> = HashMap()
) {
    init {
        val logTime = TimeUtil.getUTCTimeWithTimeZone()
        timestamp = logTime.utcTime
        timezone = logTime.timeZone
    }

    fun append(key: String, value: Map<String, Any?>?): AppLog {
        extra[key] = value
        return this
    }

    fun toJson(): String {
        return GsonBuilder()
            .excludeFieldsWithoutExposeAnnotation()
            .create()
            .toJson(this)
    }

    fun toFlattenMap(): Map<String, Any?> {
        val map = HashMap<String, Any?>()
        map["id"] = id
        map["timestamp"] = timestamp
        map["timezone"] = timezone
        map["appVersion"] = appVersion
        map["networkMedia"] = networkMedia
        map["device"] = device
        map["userInfo"] = userInfo
        map["action"] = action
        for (entry in extra.entries) {
            map[entry.key] = entry.value
        }
        return map
    }
}