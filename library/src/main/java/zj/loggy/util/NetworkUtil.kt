package zj.loggy.util

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build

object NetworkUtil {

    const val NETWORK_WIFI = "wifi"
    const val NETWORK_MOBILE = "mobile"
    const val NETWORK_UNKNOWN = "N/A"

    fun getConnectionType(context: Context?): String {
        val cm = context?.applicationContext?.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager?
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getConnectionType(cm)
        } else {
            getConnectionTypeBeforeM(cm)
        }
    }

    private fun getConnectionType(cm: ConnectivityManager?): String {
        return cm?.getNetworkCapabilities(cm.activeNetwork)?.run {
            when {
                hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> NETWORK_WIFI
                hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> NETWORK_MOBILE
                else -> NETWORK_UNKNOWN
            }
        } ?: NETWORK_UNKNOWN
    }

    @Suppress("DEPRECATION")
    private fun getConnectionTypeBeforeM(cm: ConnectivityManager?): String {
        return cm?.activeNetworkInfo?.run {
            when (type) {
                ConnectivityManager.TYPE_WIFI -> NETWORK_WIFI
                ConnectivityManager.TYPE_MOBILE -> NETWORK_MOBILE
                else -> NETWORK_UNKNOWN
            }
        } ?: NETWORK_UNKNOWN
    }
}