package zj.loggy.util

import android.content.Context
import android.os.Build
import android.util.Log
import zj.loggy.provider.LoggyContentProvider


object SystemInfoUtil {

    fun getAppVersion(context: Context? = LoggyContentProvider.mContext): String {

        val version = try {
            context?.packageManager
                ?.getPackageInfo(context.packageName, 0)
                ?.versionName ?: ""
        } catch (e: Exception) {
            debug {
                Log.e(javaClass.simpleName, e.message, e)
            }
            ""
        }

        val regex = """(\d+\.\d+\.\d+)(\.\d+)*""".toRegex()
        val result = regex.find(version)
        return result?.destructured?.component1() ?: version
    }

    fun getOsVersion(): String {
        return String.format("%s (%s)",
                Build.VERSION_CODES::class.java.fields[Build.VERSION.SDK_INT].name,
                Build.VERSION.RELEASE)
    }

    fun getModel(): String {
        return String.format("%s %s",
                Build.MANUFACTURER,
                Build.MODEL)
    }

}