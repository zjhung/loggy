package zj.loggy.model

import com.google.gson.annotations.Expose

data class Action(
    @Expose var mainCategory: String,
    @Expose var subCategory: String?,
    @Expose var info: Map<String, Any?>? = null
)