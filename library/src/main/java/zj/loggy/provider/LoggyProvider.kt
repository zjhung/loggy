package zj.loggy.provider

import android.util.Log
import com.google.gson.GsonBuilder
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import zj.loggy.db.AppLogDao
import zj.loggy.db.LoggyDataBase
import zj.loggy.db.entity.LogEntity
import zj.loggy.model.*
import zj.loggy.util.NetworkUtil
import zj.loggy.util.SystemInfoUtil
import zj.loggy.util.debug

object LoggyProvider {

    data class Config(var tableSize: Int? = null)

    private val tagName = javaClass.simpleName
    var config: Config? = null

    fun newLog(action: Action): AppLog? {
        val appLog = AppLog()
        appLog.appVersion = SystemInfoUtil.getAppVersion()
        appLog.networkMedia = NetworkUtil.getConnectionType(LoggyContentProvider.mContext)
        appLog.device = Device()
        appLog.action = action
        return appLog
    }

    suspend fun save(logs: List<AppLog>) {
        withContext(Dispatchers.IO) {
            val entities = logs.map { LogEntity(log = it.toJson()) }
            getAppLogDao()?.insertWithLimit(entities, config?.tableSize)
        }
    }

    suspend fun fetch(size: Int?): List<AppLog>? {
        return withContext(Dispatchers.IO) {
            val gson = GsonBuilder().excludeFieldsWithoutExposeAnnotation().create()
            val dao = getAppLogDao()
            val entities = size?.let { dao?.query(it) } ?: dao?.queryAll()

            entities?.map { entity ->
                gson.fromJson(entity.log, AppLog::class.java).apply {
                    id = entity.id
                }
            }
        }
    }

    suspend fun delete(logs: List<AppLog>): Int? {
        return withContext(Dispatchers.IO) {
            val entities = logs.map { LogEntity(it.id, it.toJson()) }
            getAppLogDao()?.delete(entities)
        }
    }

    suspend fun deleteAll(): Int? {
        return withContext(Dispatchers.IO) {
            getAppLogDao()?.deleteAll()
        }
    }

    private fun getAppLogDao(): AppLogDao? {
        return LoggyContentProvider.mContext?.let {
            LoggyDataBase.getInstance(it)?.appLogDao()
        } ?: run {
            debug { Log.e(tagName, "context == null") }
            null
        }
    }
}