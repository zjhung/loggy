package zj.loggy.db

import androidx.room.*
import zj.loggy.db.entity.LogEntity
import kotlin.math.max

@Dao
interface AppLogDao {

    @Query("SELECT * FROM LogEntity LIMIT :count")
    fun query(count: Int): List<LogEntity>

    @Query("SELECT * FROM LogEntity")
    fun queryAll(): List<LogEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(logs: List<LogEntity>)

    @Transaction
    fun insertWithLimit(logs: List<LogEntity>, limit: Int?) {
        insert(logs)
        limit?.let {
            limitRecordCount(max(0, limit))
        }
    }

    @Delete
    fun delete(logs: List<LogEntity>): Int

    @Query("DELETE FROM LogEntity")
    fun deleteAll(): Int

    @Query("DELETE FROM LogEntity WHERE id < (SELECT id FROM LogEntity ORDER BY id DESC LIMIT 1 OFFSET :keepSize-1)")
    fun limitRecordCount(keepSize: Int)
}