package zj.loggy.util

import org.junit.Assert.assertEquals
import org.junit.Test
import java.text.SimpleDateFormat
import java.util.*

class TimeUtilTest {

    @Test
    fun testGetUTCTimeWithTimeZone() {
        val utcFormatter = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.getDefault())
        utcFormatter.timeZone = TimeZone.getTimeZone("UTC")
        val expectedTime = utcFormatter.format(Date())

        val gmtFormatter = SimpleDateFormat("Z", Locale.getDefault())
        val expectedTimeZone = gmtFormatter.format(Date())

        val logTime = TimeUtil.getUTCTimeWithTimeZone()

        println("expected= ($expectedTime, $expectedTimeZone)")
        println("actual= (${logTime.utcTime}, ${logTime.timeZone})")

        assertEquals(expectedTime, logTime.utcTime)
        assertEquals(expectedTimeZone, logTime.timeZone)
    }
}