package zj.loggy.model

import android.os.Build
import com.google.gson.annotations.Expose

data class Device (
    @Expose val platform: String = "android",
    @Expose var model: String = Build.VERSION.RELEASE,
    @Expose var manufacturer: String = Build.MANUFACTURER,
    @Expose var version: String = Build.MODEL
)