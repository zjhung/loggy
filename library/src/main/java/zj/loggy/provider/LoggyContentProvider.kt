package zj.loggy.provider

import android.annotation.SuppressLint
import android.content.ContentProvider
import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.net.Uri
import android.telephony.PhoneStateListener
import android.telephony.TelephonyManager
import androidx.annotation.RestrictTo
import androidx.annotation.RestrictTo.Scope.LIBRARY
import zj.loggy.db.LoggyDataBase
import zj.loggy.listener.SignalChangeListener


@RestrictTo(LIBRARY)
internal class LoggyContentProvider : ContentProvider() {

    private var signalChangedListener: SignalChangeListener = SignalChangeListener()

    companion object {
        @SuppressLint("StaticFieldLeak")
        internal var mContext: Context? = null
    }

    override fun onCreate(): Boolean {
        mContext = context?.applicationContext ?: context
        registerSignalChangedListener(mContext)
        return mContext != null
    }

    override fun query(
        uri: Uri, projection: Array<String>?, selection: String?,
        selectionArgs: Array<String>?, sortOrder: String?
    ): Cursor? {
        return null
    }

    override fun getType(uri: Uri): String? {
        return null
    }

    override fun insert(uri: Uri, values: ContentValues?): Uri? {
        return null
    }

    override fun delete(uri: Uri, selection: String?, selectionArgs: Array<String>?): Int {
        return 0
    }

    override fun update(
        uri: Uri, values: ContentValues?,
        selection: String?, selectionArgs: Array<String>?
    ): Int {
        return 0
    }

    override fun shutdown() {
        super.shutdown()
        LoggyDataBase.destroyInstance()
    }

    private fun registerSignalChangedListener(context: Context?) {
        val telephony = context?.getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager?
        telephony?.listen(signalChangedListener, PhoneStateListener.LISTEN_SIGNAL_STRENGTHS)
    }
}