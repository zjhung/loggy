import com.android.build.gradle.api.LibraryVariant
import com.android.build.gradle.internal.api.LibraryVariantOutputImpl
import com.jfrog.bintray.gradle.BintrayExtension
import java.util.Properties

plugins {
    id("com.android.library")
    id("maven-publish")
    id("com.jfrog.bintray") version "1.8.5"
    kotlin("android")
    kotlin("kapt")
}

android {
    compileSdkVersion(Config.compileSdk)

    defaultConfig {
        minSdkVersion(Config.minSdk)
        targetSdkVersion(Config.targetSdk)
        versionCode = Config.versionCode
        versionName = Config.versionName
        testInstrumentationRunner = "android.support.test.runner.AndroidJUnitRunner"

        javaCompileOptions {
            annotationProcessorOptions {
                arguments["room.incremental"] = "true"
            }
        }

        kapt {
            arguments {
                arg("room.incremental", "true")
                arg("room.expandProjection", "true")
                arg("room.schemaLocation", "$rootDir/schemas")
            }
        }
    }

    setProperty("archivesBaseName", Config.artifactName)

    buildTypes {
        named("release") {
            isMinifyEnabled = false
            proguardFiles(getDefaultProguardFile("proguard-android.txt"), "proguard-rules.pro")
        }
    }
}

android.libraryVariants.all {
    generateAarName(this)
}

fun generateAarName(variant: LibraryVariant) {
    variant.outputs.filterIsInstance<LibraryVariantOutputImpl>().forEach { output ->
        output.outputFileName = "${Config.artifactName}-${variant.name}-${Config.versionName}.aar"
    }
}

dependencies {
    implementation(fileTree( mapOf("dir" to "libs", "include" to listOf("*.jar")) ))
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8:1.4.20")
    // Kotlin coroutines
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.4.1")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-android:1.4.1")

    implementation("com.google.code.gson:gson:2.8.6")

    // room
    implementation("androidx.room:room-runtime:2.2.5")
    annotationProcessor("androidx.room:room-compiler:2.2.5")
    kapt("androidx.room:room-compiler:2.2.5")
    testImplementation("androidx.room:room-testing:2.2.5")

    testImplementation("junit:junit:4.13.1")
    androidTestImplementation("com.android.support.test:runner:1.0.2")
    androidTestImplementation("com.android.support.test.espresso:espresso-core:3.0.2")
}

val sourceJar = task("sourceJar", Jar::class) {
    archiveClassifier.set("sources")
    from(android.sourceSets.getByName("main").java.srcDirs)
}

publishing {
    publications {
        create<MavenPublication>("LoggyPublication") {
            groupId = "zj.loggy"
            artifactId = Config.artifactName
            version = Config.versionName
            artifact(sourceJar)
            artifact("$buildDir/outputs/aar/${Config.artifactName}-release-${Config.versionName}.aar")

            pom.withXml {
                val dependenciesNode = asNode().appendNode("dependencies")

                project.configurations.implementation.get().allDependencies.forEach {
                    if (it.group != null) {
                        val dependencyNode = dependenciesNode.appendNode("dependency")
                        dependencyNode.appendNode("groupId", it.group)
                        dependencyNode.appendNode("artifactId", it.name)
                        dependencyNode.appendNode("version", it.version)

                        (it as? ModuleDependency)?.let { dep ->
                            if (!dep.excludeRules.isNullOrEmpty()) {
                                val exclusionsNode = dependencyNode.appendNode("exclusions")
                                dep.excludeRules.forEach { rule ->
                                    val exclusionNode = exclusionsNode.appendNode("exclusion")
                                    exclusionNode.appendNode("groupId", rule.group)
                                    exclusionNode.appendNode("artifactId", rule.module)
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    repositories {
        maven("https://dl.bintray.com/imuniquezj/android")
    }
}

fun findProperty(s: String) = project.findProperty(s) as String?

val properties = Properties()
properties.load(project.rootProject.file("local.properties").inputStream()) // load local.properties
val bintrayUser: String? = properties.getProperty("bintray.username")
val bintrayApiKey: String? = properties.getProperty("bintray.apiKey")

bintray {
    user = findProperty("bintrayUser") ?: bintrayUser
    key = findProperty("bintrayApiKey") ?: bintrayApiKey
    publish = true
    setPublications("LoggyPublication")
    pkg(delegateClosureOf<BintrayExtension.PackageConfig> {
        repo = "android"
        name = Config.artifactName
        publicDownloadNumbers = true
        vcsUrl = "https://bitbucket.org/zjhung/loggy.git"
        setLabels("kotlin")
        setLicenses("Apache-2.0")
        version(delegateClosureOf<BintrayExtension.VersionConfig> {
            name = Config.versionName
        })
    })
}