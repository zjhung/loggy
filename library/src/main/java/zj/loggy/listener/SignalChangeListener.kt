package zj.loggy.listener

import android.telephony.PhoneStateListener
import android.telephony.SignalStrength

class SignalChangeListener: PhoneStateListener() {

    companion object {
        var strength: Int? = null
    }

    override fun onSignalStrengthsChanged(signalStrength: SignalStrength) {
        strength = signalStrength.level
    }

}