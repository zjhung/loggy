package zj.loggy.util

import zj.loggy.BuildConfig


inline fun debug(block: () -> Unit) {
    if (BuildConfig.DEBUG) {
        block()
    }
}