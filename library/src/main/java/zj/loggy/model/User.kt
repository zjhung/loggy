package zj.loggy.model

import com.google.gson.annotations.Expose

data class User(@Expose var memberID: String? = null, @Expose var memberGUID: String? = null)